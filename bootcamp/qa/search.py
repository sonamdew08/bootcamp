from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Search
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from . import models
from taggit.managers import TaggableManager


connections.create_connection(hosts="b00b7982318e4b0fb850198f58ee9506.us-east-1.aws.found.io", http_auth="elastic:7gncJNCvkJWKS9BkG1HHYc8B")
# connections.create_connection()
# connections.create_connection(
#  hosts=['https://b00b7982318e4b0fb850198f58ee9506.us-east-1.aws.found.io:9243'], timeout=20)

class QuestionIndex(DocType):
    title = Text()
    content = Text()
       
    class Meta:
        index = 'questions-index'


def bulk_indexing():
    # QuestionIndex.init()
    # es = Elasticsearch()
    # es = Elasticsearch(hosts="b00b7982318e4b0fb850198f58ee9506.us-east-1.aws.found.io", http_auth="elastic:7gncJNCvkJWKS9BkG1HHYc8B")
    # bulk(client=es, actions=(b.indexing() for b in models.Question.objects.all().iterator()))
    for b in models.Question.objects.all():
           b.indexing()


def search(title):
    s = Search(index = 'questions-index').filter('query_string', query = '*'+ title +'*',
                                        fields = ['title','content'])

    response = s.execute()
    title_response = [query['title'] for query in response]
    return title_response