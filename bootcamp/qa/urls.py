from django.conf.urls import url

from bootcamp.qa import views

app_name = 'qa'
urlpatterns = [
    url(r'^$', views.QuestionListView.as_view(), name='index_noans'),
    url(r'^answered/$', views.QuestionAnsListView.as_view(), name='index_ans'),
    url(r'^indexed/$', views.QuestionsIndexListView.as_view(), name='index_all'),
    url(r'^question-detail/(?P<pk>\d+)/$', views.QuestionDetailView.as_view(), name='question_detail'),
    url(r'^ask-question/$', views.CreateQuestionView.as_view(), name='ask_question'),
    # url(r'^edit-question/(?P<pk>\d+)/$', views.edit_question, name='edit_question'),
    url(r'^edit-question/(?P<question_id>\d+)/$', views.UpdateQuestionView.as_view(), name='edit_question'),
    url(r'^propose-answer/(?P<question_id>\d+)/$', views.CreateAnswerView.as_view(), name='propose_answer'),
    url(r'^question/vote/$', views.question_vote, name='question_vote'),
    url(r'^answer/vote/$', views.answer_vote, name='answer_vote'),
    url(r'^accept-answer/$', views.accept_answer, name='accept_answer'),
    url(r'^update-answer/(?P<pk>[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12})/$', views.UpdateAnswerView.as_view(), name='update_answer'),
    url(r'^delete-answer/(?P<pk>[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12})/$', views.DeleteAnswerView.as_view(), name='delete_answer'),
    url(r'^download/$', views.download, name='download'),
    url(r'^downloads/question/(?P<user_id>\d+)$', views.download_question, name='download_question'),
    url(r'^downloads/question/$', views.get_download_files_status, name='download_question_status'),
    url(r'^download/questions/files/(?P<user_id>\d+)/$', views.download_files, name='download_files'),
]
