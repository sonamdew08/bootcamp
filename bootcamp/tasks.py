import json
from celery import shared_task
from bootcamp.qa.models import Question
from celery.utils.log import get_task_logger
import os

logger = get_task_logger(__name__)

@shared_task
def create_download_file(user_id):

    filename = str(user_id)+'.json'
      
    file_path = os.path.join(os.environ.get('HOME'), filename) 
    logger.info(file_path)  
    questions = [Question.objects.filter(user=user_id)]
    logger.info(questions)  
    question_list = []
    for question in questions:
        question_list.append({"Title": question.title, "Question": question.content})    

    logger.info(question_list)    
    with open(filename, "w+") as f:
        json.dump(question_list, f)

